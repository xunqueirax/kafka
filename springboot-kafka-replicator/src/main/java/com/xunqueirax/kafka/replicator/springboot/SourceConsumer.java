package com.xunqueirax.kafka.replicator.springboot;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Properties;
@Component
public class SourceConsumer {
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConsumer.class);
	@Value("${application.id}")
	private String appId;
	@Value("${source.bootstrapServers}")
	private String source;
	@Value("${source.topic.name}")
	private String topic;
	@Autowired
	private TargetProducer targetProducer;
	private KafkaStreams streams;

	@EventListener(ContextRefreshedEvent.class)
	public void initStream() {
		LOGGER.info("Initialising Kafka streams {}", this);
		closeResources();

		Properties props = new Properties();
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, appId);
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, source);
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		final StreamsBuilder builder = new StreamsBuilder();
		KStream<String,String> stream = builder.stream(topic);
		stream.foreach((s, s2) -> targetProducer.produce(s, s2));
		final Topology topology = builder.build();
		LOGGER.debug(topology.describe().toString());
		streams = new KafkaStreams(topology, props);
		streams.start();
	}

	@EventListener(ContextClosedEvent.class)
	public void closeResources() {
		if (streams != null) {
			LOGGER.info("Closing Kafka consumer's Streams; object state is {}", String.valueOf(streams.state()));
			streams.close();
		}
	}

	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public KafkaStreams getStreams() {
		return streams;
	}
	public void setStreams(KafkaStreams streams) {
		this.streams = streams;
	}
	public TargetProducer getTargetProducer() {
		return targetProducer;
	}
	public void setTargetProducer(TargetProducer targetProducer) {
		this.targetProducer = targetProducer;
	}
}
