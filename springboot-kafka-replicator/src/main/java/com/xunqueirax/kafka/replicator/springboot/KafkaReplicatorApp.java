package com.xunqueirax.kafka.replicator.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaReplicatorApp {

	public static void main(String[] args) {
		SpringApplication.run(KafkaReplicatorApp.class, args);
	}

}
