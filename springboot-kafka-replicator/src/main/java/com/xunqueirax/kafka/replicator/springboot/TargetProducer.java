package com.xunqueirax.kafka.replicator.springboot;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class TargetProducer {
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConsumer.class);
	@Value("${target.bootstrapServers}")
	private String target;
	@Value("${target.topic.name}")
	private String topic;
	private KafkaProducer<String,String> producer;

	@EventListener(ContextRefreshedEvent.class)
	public void initProducer() {
		LOGGER.info("Initialising Kafka producer");
		closeResources();
		Properties properties = new Properties();
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, target);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		producer = new KafkaProducer<>(properties);
	}
	@EventListener(ContextClosedEvent.class)
	public void closeResources() {
		if ( producer != null) {
			LOGGER.info("Closing Kafka producer {}", producer.toString());
			producer.flush();
			producer.close();
		}
	}
	public void produce(String key, String data) {
		ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topic, key, data);
		producer.send(producerRecord);
	}

	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public KafkaProducer<String, String> getProducer() {
		return producer;
	}
	public void setProducer(KafkaProducer<String, String> producer) {
		this.producer = producer;
	}
}
