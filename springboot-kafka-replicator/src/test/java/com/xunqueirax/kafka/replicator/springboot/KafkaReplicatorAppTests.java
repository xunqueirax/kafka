package com.xunqueirax.kafka.replicator.springboot;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class KafkaReplicatorAppTests {
	@Autowired
	private SourceConsumer consumer;

	@Test
	void contextLoads() {
		assertEquals("KafkaReplicatorTests", consumer.getAppId());
		assertNotNull(consumer.getSource());
		assertNotNull(consumer.getTopic());
		assertNotNull(consumer.getTargetProducer().getTarget());
		assertNotNull(consumer.getTargetProducer().getTopic());
		assertNotNull(consumer.getTargetProducer().getProducer());
	}
}
