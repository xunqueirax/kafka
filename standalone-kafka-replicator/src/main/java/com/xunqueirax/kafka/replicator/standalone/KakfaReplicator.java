package com.xunqueirax.kafka.replicator.standalone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * A standalone application that reads from a topic in a Kafka cluster
 * and replicates its contents to another topic in another cluster.
 */
public class KakfaReplicator {
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConsumer.class);
	public static void main(String[] args) {

		AppProperties props = null;

		try {
			props = new AppProperties();
		}catch (FileNotFoundException e) {
			LOGGER.error("Properties file not found {}", e.getMessage());
			System.exit(3);
		} catch (IOException e) {
			LOGGER.error("Problem reading properties file {}", e.getMessage());
			System.exit(2);
		}
		final TargetProducer producer = new TargetProducer(props);
		final SourceConsumer consumer = new SourceConsumer(producer, props);
		final CountDownLatch latch = new CountDownLatch(1);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
			@Override
			public void run() {
				if (consumer != null)
					consumer.close();
				if (producer != null)
					producer.close();
			}
		});
		try {
			consumer.start();
			latch.await();
		} catch (Throwable e) {
			System.exit(1);
		}
		System.exit(0);
	}
}
