package com.xunqueirax.kafka.replicator.standalone;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AppProperties {

	private final Properties appProperties;

	public AppProperties() throws IOException {
		String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		String appConfigPath = rootPath + "application.properties";
		appProperties = new Properties();
		appProperties.load(new FileInputStream(appConfigPath));
	}

	public String getProperty(String key) {
		return appProperties.getProperty(key);
	}
	public String getProperty(String key, String defaultVal) {
		return appProperties.getProperty(key, defaultVal);
	}
}
