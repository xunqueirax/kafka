package com.xunqueirax.kafka.replicator.standalone;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class TargetProducer {
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConsumer.class);
	private String target;
	private String topic;
	private KafkaProducer<String, String> producer;

	public TargetProducer(AppProperties props) {
		LOGGER.info("Initialising Kafka producer");
		target = props.getProperty("target.bootstrapServers");
		topic = props.getProperty("target.topic.name");
		Properties kProps = new Properties();
		kProps.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, target);
		kProps.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		kProps.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		producer = new KafkaProducer<>(kProps);
	}

	public void close() {
		if (producer != null) {
			LOGGER.info("Closing Kafka producer {}", producer.toString());
			producer.flush();
			producer.close();
		}
	}

	public void produce(String key, String data) {
		ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topic, key, data);
		producer.send(producerRecord);
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public KafkaProducer<String, String> getProducer() {
		return producer;
	}

	public void setProducer(KafkaProducer<String, String> producer) {
		this.producer = producer;
	}
}
