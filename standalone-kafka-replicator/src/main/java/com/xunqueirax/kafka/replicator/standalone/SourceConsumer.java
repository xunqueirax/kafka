package com.xunqueirax.kafka.replicator.standalone;


import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class SourceConsumer {
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConsumer.class);
	private String appId;
	private String source;
	private String topic;
	private TargetProducer targetProducer;
	private KafkaStreams streams;

	public SourceConsumer(TargetProducer targetProducer, AppProperties props) {
		LOGGER.info("Initialising Kafka streams {}", this);
		appId = props.getProperty("application.id");
		source = props.getProperty("source.bootstrapServers");
		topic = props.getProperty("source.topic.name");
		Properties kProps = new Properties();
		kProps.put(StreamsConfig.APPLICATION_ID_CONFIG, appId);
		kProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, source);
		kProps.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		kProps.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		final StreamsBuilder builder = new StreamsBuilder();
		KStream<String, String> stream = builder.stream(topic);
		stream.foreach((s, s2) -> targetProducer.produce(s, s2));
		final Topology topology = builder.build();
		LOGGER.debug(topology.describe().toString());
		streams = new KafkaStreams(topology, kProps);
	}

	public void start() {
		streams.start();
	}

	public void close() {
		if (streams != null) {
			LOGGER.info("Closing Kafka consumer's Streams; object state is {}", String.valueOf(streams.state()));
			streams.close();
		}
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public KafkaStreams getStreams() {
		return streams;
	}

	public void setStreams(KafkaStreams streams) {
		this.streams = streams;
	}

	public TargetProducer getTargetProducer() {
		return targetProducer;
	}

	public void setTargetProducer(TargetProducer targetProducer) {
		this.targetProducer = targetProducer;
	}
}
